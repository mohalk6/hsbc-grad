public class CurrentAccountRegister {

    private static CurrentAccount[] currentAccount = new CurrentAccount[] {
        new CurrentAccount("Rajesh", "C&G", "AB@#41367D"), new CurrentAccount("Neena", "LTI", "EHT6452!6"),
        new CurrentAccount("Naveen", "ASBA", "ERT5367&$2", new Address("5th Ave", "Mumbai", 447142))

    };

    public static CurrentAccount fetchCurrentAccountByAccountId(long accountId) {

        for (CurrentAccount currentAccount : currentAccount) {
            if (currentAccount.getAccountNumber() == accountId) {
                return currentAccount;
            }
        }
        return null;
    }
}
