public class CurrentAccountClient{

    public static void main(String[] args) {
        CurrentAccount rajesh = new CurrentAccount("Rajesh", "C&G", "AB@#41367D");
        Address address = new Address("5th Ave", "Mumbai", 447142);
        CurrentAccount naveen = new CurrentAccount("Naveen", "ASBA", "ERT5367&$2", address);
        Address address1 = new Address("11th Ave", "Pune", 947142);
        CurrentAccount vinay = new CurrentAccount("Vinay", "KR Textiles", "GSHAJ3&$2", 78300.00, address1);
        System.out.println("Customer Name: " + rajesh.getCustomerName());
        System.out.println("Account Number " + rajesh.getAccountNumber());
        System.out.println("Initial Account balance " + rajesh.checkBalance());
        System.out.println("Business Name " + rajesh.getBusinessName());
        System.out.println("GST Number " + rajesh.getGstNumber());

        System.out.println("Account Balance " + rajesh.deposit(5000));
        System.out.println("Balance after deposit " + rajesh.checkBalance());
        rajesh.withdraw(200);
        System.out.println("Balance after Withdraw " + rajesh.checkBalance());
        
        long accountId = naveen.getAccountNumber();
        // System.out.println("Balance after transfer "+ rajesh.transferAmount(500, accountId) );
        rajesh.transferAmount(500, accountId);
        System.out.println("Balance after transfer " + rajesh.checkBalance());


        System.out.println(" ---------------------------------");

        
        System.out.println("Account Number " + naveen.getAccountNumber());
        System.out.println("Customer Name: " + naveen.getCustomerName());
        System.out.println("Business Name " + naveen.getBusinessName());
        System.out.println("GST Number " + naveen.getGstNumber());
        System.out.println("Initial Account balance " + naveen.checkBalance());
        System.out.println("Account Balance " + naveen.deposit(4000));
        System.out.println("Balance after deposit" + naveen.checkBalance());
        naveen.withdraw(2000);
        System.out.println("Balance after Withdraw " + naveen.checkBalance());

        System.out.println(" ---------------------------------");

        
        System.out.println("Customer Name: " + vinay.getCustomerName());

        
        

    }


}