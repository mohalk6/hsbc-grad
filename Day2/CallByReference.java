public class CallByReference{

    public static void main(String[] args) {

        int array[] = new int[] { 20, 24, 61, 87, 14 };

        System.out.println("Changes before the call ");
        for (int a : array) {
            System.out.print(""+a + " ");
        }

        System.out.println("\n"+"-------------------------------");
        callByRef(array);
        System.out.println("\n"+"-------------------------------");
        System.out.println("Changes after the call ");
        for (int a : array) {
            System.out.print(""+a + " ");
        }
    }


    private static void callByRef(int[] arr) {
        System.out.println("Changes inside the method");

        arr[0] = 44;
        arr[1] = 55;
        arr[2] = 66;
        arr[3] = 77;

        for (int a : arr) {
            System.out.print(""+a + " ");
        }


    }
}
