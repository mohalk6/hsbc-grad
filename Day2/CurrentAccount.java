public class CurrentAccount{

    private static long accountNumberTracker = 10000;

    private long accountNumber;

    private double accountBalance;

    private String gstNumber;

    private String customerName;

    private String businessName;

    private Address address;

    public long getAccountNumber() {
        return this.accountNumber;
    }

    public String getBusinessName(){
        return this.businessName;
    }

    public String getGstNumber(){
        return this.gstNumber;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public CurrentAccount(String customerName, String businessName, String gstNumber)
    {
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.accountNumber = ++ accountNumberTracker;
        this.accountBalance = 50000;

    }

    public CurrentAccount(String customerName, String businessName, String gstNumber, Address address) {
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.accountNumber = ++ accountNumberTracker;
        this.accountBalance = 50000;
        this.address = address;
    }

    public CurrentAccount(String customerName, String businessName, String gstNumber, Double accountBalance, Address address) {
        this.customerName = customerName;
        this.businessName = businessName;
        this.gstNumber = gstNumber;
        this.accountNumber = ++ accountNumberTracker;
        this.address = address;
        this.accountBalance = accountBalance + 50000;
    }    

    public double withdraw(double amount){
        if( (this.accountBalance > 50000) && (this.accountBalance >= amount)) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    public double deposit(double amount, CurrentAccount user) {
        user.accountBalance = user.accountBalance + amount;
        return accountBalance;
    }

    public double deposit(double amount, String notes) {
        this.accountBalance = accountBalance + amount;
        System.out.println("notes: " + notes);
        return accountBalance;
    }

    // public double transferAmount(double amount, CurrentAccount user){
    //     if(this.accountBalance>amount)
    //     {
    //         double amountWithdrawn = this.withdraw(amount);
    //         user.deposit(amountWithdrawn);
    //         return this.accountBalance;
            
    //     }
    //     return 0;
    // }
    
    public void transferAmount(double amount, long userAccountId) {
        
        if(this.accountBalance>amount)
        {
            double amountWithdrawn = this.withdraw(amount);
            CurrentAccount userAccount = CurrentAccountRegister.fetchCurrentAccountByAccountId(userAccountId);
            if(userAccount!= null){
                userAccount.deposit(amountWithdrawn);

            }
            
        }
    }





}