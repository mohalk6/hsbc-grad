public class CallByValue {

    public static void main(String[] args) {

        int operand1 = 10;
        int operand2 = 12;

        System.out.println("Changes before the call " +" " + operand1 +" "+ operand2);
        callByValue(operand1, operand2);
        System.out.println("Changes after the call " +" " + operand1 +" "+ operand2);
        

       
    }

    private static void callByValue(int operand1, int operand2) {
        operand1 = operand1 + 22;
        operand2 = operand2 + 22;

        System.out.println("Local changes applied  "+" " + operand1 +" "+ operand2);

    }

 
}
