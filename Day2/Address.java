public class Address{

    private String streetName;

    private String cityName;

    private int zipCode;

    public Address(String streetName, String cityName, int zipCode)
    {
        this.streetName = streetName;
        this.cityName = cityName;
        this.zipCode = zipCode;
    
    }

    public String getStreet() {
        return streetName;
    }

    public void setStreet(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return cityName;
    }

    public void setCity(String cityName) {
        this.cityName = cityName;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

}