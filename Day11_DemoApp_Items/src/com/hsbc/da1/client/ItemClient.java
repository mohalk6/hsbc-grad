package com.hsbc.da1.client;

import java.util.Collection;

import com.hsbc.da1.exceptions.ItemNotFoundException;
import com.hsbc.da1.model.Item;
import com.hsbc.da1.service.ItemService;
import com.hsbc.da1.service.ItemServiceImpl;

public class ItemClient {

	public static void main(String[] args) {
		
		//ItemService itemService = new ItemServiceImpl();
		
		/*STEP 1
		Item itCrackle = itemService.saveItem("DairyMilk Crackle", 80);
		System.out.println(itCrackle);
		Item itSilk = itemService.saveItem("DairyMilk Silk", 170);
		System.out.println(itSilk);
		Item itOreo = itemService.saveItem("DairyMilk Oreo", 180);
		System.out.println(itOreo);
		Item itCaramel = itemService.saveItem("DairyMilk Caramel", 145);
		System.out.println(itCaramel);
		Item itNuts = itemService.saveItem("DairyMilk Nuts", 65);
		System.out.println(itNuts);
		*/
		
		/* STEP 2
		System.out.println("\n\n\n-------------------------------FETCHING ITEM BY ITEM ID-----------------------\n\n\n");
		try {
		Item crackleFetched =itemService.fetchItemByItemId(18);
		System.out.println(crackleFetched);
		}catch(ItemNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		*/
		
		/*STEP 3
		System.out.println("\n\n\n-------------------------------FETCHING ITEM BY ITEM NAME-----------------------\n\n\n");
		try {
		Item crackleFetched =itemService.fetchItemByItemName("DairyMilk Crackle");
		System.out.println(crackleFetched);
		}catch(ItemNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		*/
		
		/*STEP 4
		System.out.println("\n\n\n------------------------------- DELETING ACCOUNT BY ACCCOUNT ID-----------------------\n\n\n");
		try {
			itemService.deleteByItemId(18);
			}catch(ItemNotFoundException e)
			{
				System.out.println(e.getMessage());
			}
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		*/
		
		/*
		System.out.println("\n\n\n------------------------------- FETCHING ALL ITEMS-----------------------\n\n\n");
		Collection<Item> items =itemService.fetchItems();
		for(Item index : items)
		{
			if(index==null)
			{
				break;
			}
			else
			{
				System.out.println(index);
			}
		}
		System.out.println("\n\n\n----------------------------------------------------------------------------------------\n\n\n");
		*/
		
		
	}
}
