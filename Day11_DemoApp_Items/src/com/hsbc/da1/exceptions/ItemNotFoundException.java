package com.hsbc.da1.exceptions;


public class ItemNotFoundException extends Exception {
	
	public ItemNotFoundException(String message) {
		super(message);
	}

	public String getMessage() {
		return super.getMessage();
	}
}
