package com.hsbc.da1.dao;
import java.util.List;

import com.hsbc.da1.exceptions.ItemNotFoundException;
import com.hsbc.da1.model.Item;


public interface ItemDAO {

	
	Item saveItem(Item item);
	
	List<Item> fetchItems();
	
	Item fetchItemByItemName(String itemName)throws ItemNotFoundException;
	
	Item fetchItemByItemId(long id) throws ItemNotFoundException;
	
	void deleteByItemId(long id) throws ItemNotFoundException;

	static ItemDAO getInstance() {
		return new ItemDAOImpl();
	}
	
	
}
