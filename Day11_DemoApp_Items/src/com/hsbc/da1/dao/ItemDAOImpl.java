package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hsbc.da1.exceptions.ItemNotFoundException;
import com.hsbc.da1.model.Item;

public class ItemDAOImpl implements ItemDAO {
	
	private static String connectString = "jdbc:derby://localhost:1527/demodb";
	private static String username = "admin";
	private static String password = "password";

	private static final String INSERT_ITEM = "insert into item (name, price ) values (?,?)";
	private static final String FETCH_ITEMS = "select * from item";
	private static final String SELECT_BY_NAME = "select *  from item where name= ?";
	private static final String SELECT_BY_ID = "select *  from item where id= ?";
	private static final String DELETE_BY_ID_QUERY = "delete  from item where id=?";

	//private static List<Item> items = new ArrayList<>();
	
	
	private static Connection getDBConnection() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Item saveItem(Item item) {
		int numberOfRecordsUpdated = 0;
		//System.out.println("Name : "+ item.getName()+"price :"+ item.getPrice());
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(INSERT_ITEM);) {
			pStmt.setString(1, item.getName());
			pStmt.setDouble(2, item.getPrice());
			numberOfRecordsUpdated = pStmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (numberOfRecordsUpdated == 1) {
			try {
				Item fetchedItem = fetchItemByItemName(
						item.getName());
				if (fetchedItem != null) {
					return fetchedItem;
				}

			} catch (ItemNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public List<Item> fetchItems() {
		Connection dbConnection = getDBConnection();
		List<Item> items = new ArrayList<>();
		try {
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(FETCH_ITEMS);
			while (rs.next()) {
				long itemId = rs.getLong("id");
				String name = rs.getString("name");
				double price = rs.getDouble("price");
				items.add(new Item(itemId, name, price));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return this.items;
		return items;
	}
	
	public Item fetchItemByItemName(String itemName)throws ItemNotFoundException{
		
		try {
			Connection connection = getDBConnection();
			PreparedStatement ps = connection.prepareStatement(SELECT_BY_NAME);
			ps.setString(1, itemName);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				//System.out.println("Account number: "+rs.getInt("account_number"));
				
				Item item = new Item(rs.getLong("id"),
						rs.getString("name"), rs.getDouble("price"));
				System.out.println("Before returning the item back to the customer");
				//System.out.println(savingsAccount);
				return item;
			}
			throw new ItemNotFoundException(" Item with " + itemName + " does not exists");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Item fetchItemByItemId(long id)throws ItemNotFoundException {
		try {
			Connection connection = getDBConnection();
			PreparedStatement ps = connection.prepareStatement(SELECT_BY_ID);
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				//System.out.println("Account number: "+rs.getInt("account_number"));
				
				Item item = new Item(rs.getLong("id"),
						rs.getString("name"), rs.getDouble("price"));
				System.out.println("Before returning the item back to the customer");
				//System.out.println(savingsAccount);
				return item;
			}
			throw new ItemNotFoundException(" Item with id : " + id + " does not exists");
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return null;
	}

	@Override
	public void deleteByItemId(long id) throws ItemNotFoundException {
		int numberOfRecordsDeleted = 0;
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(DELETE_BY_ID_QUERY);) {
			pStmt.setLong(1,id);
			numberOfRecordsDeleted = pStmt.executeUpdate();
			if(numberOfRecordsDeleted==1)
			{
				System.out.println("The item with id : "+id+" was succssfully deleted");
			}
			else
			{
			throw new ItemNotFoundException(" Item with id : " + id + " does not exists");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
