package com.hsbc.da1.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.da1.model.User;

public class AgeCalculatorServlet extends HttpServlet {
	
	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes) throws IOException {
		
		String dobStr = httpServletReq.getParameter("dob");
		
		PrintWriter out = httpServletRes.getWriter();
		
		User user = (User)httpServletReq.getSession().getAttribute("user");
		
		System.out.println(" User details in Age Calculator servlet "+ user);
		
		//calculate the age in days ....
		int days = 340;
				
		out.write("<h1> You are  :"+days+ "old!! </h1>");
	}

}
