package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.exceptions.ItemNotFoundException;
import com.hsbc.da1.model.Item;

public interface ItemService {

	static final ItemService itemService = new ItemServiceImpl();
	Item saveItem(Item item);
	
	Item saveItem(String itemName,double Price);

	List<Item> fetchItems();
	
	Item fetchItemByItemId(long id) throws ItemNotFoundException;

	static ItemService getItemService() {
		return new ItemServiceImpl();
	}

	Item fetchItemByItemName(String name)throws ItemNotFoundException;
	
	void deleteByItemId(long id) throws ItemNotFoundException;
}
