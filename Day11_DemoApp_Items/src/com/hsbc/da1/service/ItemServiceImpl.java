package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.dao.ItemDAO;
import com.hsbc.da1.exceptions.ItemNotFoundException;
import com.hsbc.da1.model.Item;

public class ItemServiceImpl implements ItemService {
	
	private ItemDAO itemDAO = ItemDAO.getInstance();

	@Override
	public Item saveItem(Item item) {
		// TODO Auto-generated method stub
		return this.itemDAO.saveItem(item);
	}
	
	@Override
	public List<Item> fetchItems() {
		// TODO Auto-generated method stub
		return itemDAO.fetchItems();
	}

	@Override
	public Item saveItem(String itemName, double Price) {
		Item it =  new Item(itemName,Price);
		
		Item ItemCreated = this.itemDAO.saveItem(it);
		return ItemCreated;
	}

	@Override
	public Item fetchItemByItemId(long id) throws ItemNotFoundException {
		
		return itemDAO.fetchItemByItemId(id);
	}

	@Override
	public Item fetchItemByItemName(String name) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		return itemDAO.fetchItemByItemName(name);
	}

	@Override
	public void deleteByItemId(long id) throws ItemNotFoundException {
		this.itemDAO.deleteByItemId(id);
		
	}

}
