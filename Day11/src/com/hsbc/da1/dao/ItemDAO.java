package com.hsbc.da1.dao;

import java.util.List;

import com.hsbc.da1.model.Item;

public interface ItemDAO {

	
	Item saveItem(Item item);
	
	List<Item> fetchItems();

	static ItemDAO getInstance() {
		return new ItemDAOImpl();
	}
}
