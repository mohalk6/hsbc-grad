package com.hsbc.da1.client;

import java.util.List;
import java.util.Scanner;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.SavingsAccountDAOFactory;
import com.hsbc.da1.util.SavingsAccountServiceFactory;
import com.hsbc.da1.exception.*;

public class SavingsAccountClient {

	public static void main(String[] args) {

		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Please enter your option ==> ");
		System.out.println("1 => Array Backed");
		System.out.println("2 => ArrayList Backed");
		System.out.println("3 => LinkedList Backed");
		
		System.out.println("======================================");
		
		int option = scanner.nextInt();
		
		SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO(option);
		SavingsAccountService savingsAccountService = SavingsAccountServiceFactory.getInstance(dao);
		

		SavingsAccountController controller = new SavingsAccountController(savingsAccountService);
		List<SavingsAccount> savingsAccounts = controller.fetchSavingsAccounts();
		SavingsAccount rajSavingsAccount = controller.openSavingsAccount("Raj", 25000);
		SavingsAccount kiranSavingsAccount = controller.openSavingsAccount("Kiran", 50000);
		SavingsAccount riyaSavingsAccount = controller.openSavingsAccount("Riya", 40000, "Pune", "Maharashtra", 411019);

//		System.out.println(""+savingsAccount1.getAccountNumber()+"\n"+savingsAccount1.getCustomerName()+"\n"
//				+savingsAccount1.getAccountBalance());
//		System.out.println(""+savingsAccount2.getAccountNumber()+"\n"+savingsAccount2.getCustomerName()+"\n"
//				+savingsAccount2.getAccountBalance());
		for (SavingsAccount savingsAccount : savingsAccounts) {
			if (savingsAccount != null) {
				System.out.println("Account Id: " + savingsAccount.getAccountNumber());
				System.out.println("Customer Name: " + savingsAccount.getCustomerName());
				System.out.println("Account Balance: " + savingsAccount.getAccountBalance());
			}
		}
		System.out.println("----------------TRANSFER----------------------");
		try {
			controller.transfer(rajSavingsAccount.getAccountNumber(), kiranSavingsAccount.getAccountNumber(), 1000);
			System.out.println("" + rajSavingsAccount.getAccountNumber() + " " + rajSavingsAccount.getAccountBalance());
			System.out.println(
					"" + kiranSavingsAccount.getAccountNumber() + " " + kiranSavingsAccount.getAccountBalance());
		} catch (CustomerNotFound customerNotFound) {
			System.out.println(customerNotFound.getMessage());
		} catch (InsufficientBankBalance insufficientBalance) {
			System.out.println(insufficientBalance.getMessage());
		}
		System.out.println("-----------------DEPOSIT---------------------");

		try {

			double rajAccountBalance = controller.deposit(rajSavingsAccount.getAccountNumber(), 26000);
			System.out.println("" + rajSavingsAccount.getAccountNumber() + " " + rajSavingsAccount.getAccountBalance());
		} catch (CustomerNotFound customerNotFound) {
			System.out.println(customerNotFound.getMessage());
		}

		System.out.println("-----------------WITHDRAW-------------------");
		try {
			double kiranAccountBalance = controller.withdraw(kiranSavingsAccount.getAccountNumber(), 24000);
			System.out.println(
					"" + kiranSavingsAccount.getAccountNumber() + " " + kiranSavingsAccount.getAccountBalance());

		} catch (CustomerNotFound customerNotFound) {
			System.out.println(customerNotFound.getMessage());
		} catch (InsufficientBankBalance insufficientBalance) {
			System.out.println(insufficientBalance.getMessage());
		}

	}

}
