package com.hsbc.da1.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.hsbc.da1.model.SavingsAccount;

public class HashMapDemo {
	
	public static void main(String[] args) {
		Map<String, SavingsAccount> mapOfSavingsAccount = new TreeMap<>();
		SavingsAccount deepa = new SavingsAccount("Deepa", 45000.0);
		SavingsAccount veena = new SavingsAccount("Veena", 5500.56);
		SavingsAccount harsha = new SavingsAccount("Harsha", 65000.23);
		SavingsAccount vinay = new SavingsAccount("Vinay", 85000.65);
		SavingsAccount kuldeep = new SavingsAccount("Kuldeep", 105000.0);

		
		mapOfSavingsAccount.put("Deepa",deepa);
		mapOfSavingsAccount.put("Veena",veena);
		mapOfSavingsAccount.put("Harsha",harsha);
		mapOfSavingsAccount.put("Vinay",vinay);
		mapOfSavingsAccount.put("Kuldeep",kuldeep);
		
		
		//System.out.printf("Size of the map is %d %n", mapOfSavingsAccount.size());
		
		//System.out.println(" Is Kuldeep present "+ ( mapOfSavingsAccount.containsKey(1004)));
		
		Set<String> setOfKeys = mapOfSavingsAccount.keySet();
		
		Iterator<String> keys = setOfKeys.iterator();
		
		while (keys.hasNext()) {
			String key = keys.next();
//			System.out.printf(" Key is %d  %s %n", key, mapOfSavingsAccount.get(key));
		}
		Collection<SavingsAccount> savingsAccounts = mapOfSavingsAccount.values();
		
		Iterator<SavingsAccount> its = savingsAccounts.iterator();
		
		Set<Map.Entry<String, SavingsAccount>> setOfEntries = mapOfSavingsAccount.entrySet();
		
		Iterator<Map.Entry<String,SavingsAccount>> iter = setOfEntries.iterator();
		
		while(iter.hasNext()) {
			Map.Entry<String, SavingsAccount> entry = iter.next();
			System.out.printf(" Key is %s  and value is %s %n", entry.getKey(), entry.getValue());
		}
	}
}
