package com.hsbc.da1.model;

import java.util.HashSet;
import java.util.*;

public class SavingsAccountDemo {

	public static void main(String args[]) {

		SavingsAccount savingsAccountRaj = new SavingsAccount("raj", 2550.0);
		savingsAccountRaj.setAccountNumber(12345);

		SavingsAccount savingsAccountPriya = new SavingsAccount("priya", 4550.0);
		savingsAccountPriya.setAccountNumber(23451);

		Set<SavingsAccount> savingsAccountSet = new HashSet<>();

		savingsAccountSet.add(savingsAccountRaj);
		savingsAccountSet.add(savingsAccountPriya);

		SavingsAccount savingsAccountPriya1 = new SavingsAccount("priya", 4550.0);
		savingsAccountPriya.setAccountNumber(125498);
		savingsAccountSet.add(savingsAccountPriya1);

		Iterator<SavingsAccount> it = savingsAccountSet.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
		}

	}

}
