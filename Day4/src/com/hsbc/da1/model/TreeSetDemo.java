package com.hsbc.da1.model;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		SavingsAccount deepa = new SavingsAccount("Deepa", 45000.0);
		SavingsAccount veena = new SavingsAccount("Veena", 5500.56);
		SavingsAccount harsha = new SavingsAccount("Harsha", 65000.23);
		SavingsAccount vinay = new SavingsAccount("Vinay", 85000.65);
		SavingsAccount kuldeep = new SavingsAccount("Kuldeep", 105000.0);

		Set<SavingsAccount> set = new TreeSet<>(new SortSAByAccountNameDesc());
		set.add(kuldeep);
		set.add(deepa);
		set.add(veena);
		set.add(vinay);
		set.add(harsha);

		Iterator<SavingsAccount> it = set.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}
}

class SortSAByAccountNumberAsc implements Comparator<SavingsAccount> {
	@Override
	public int compare(SavingsAccount sa1, SavingsAccount sa2) {
		return (int) (sa1.getAccountNumber() - sa2.getAccountNumber());
	}
}

class SortSAByAccountNumberDesc implements Comparator<SavingsAccount> {
	@Override
	public int compare(SavingsAccount sa1, SavingsAccount sa2) {
		return -1 * (int) (sa1.getAccountNumber() - sa2.getAccountNumber());
	}
}

class SortSAByAccountNameAsc implements Comparator<SavingsAccount> {
	@Override
	public int compare(SavingsAccount sa1, SavingsAccount sa2) {
		return sa1.getCustomerName().compareTo(sa2.getCustomerName());
	}
}

class SortSAByAccountNameDesc implements Comparator<SavingsAccount> {
	@Override
	public int compare(SavingsAccount sa1, SavingsAccount sa2) {
		return sa2.getCustomerName().compareTo(sa1.getCustomerName());
	}
}
