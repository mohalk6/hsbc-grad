package com.hsbc.da1.util;

import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.service.*;

public class SavingsAccountServiceFactory {

	public static SavingsAccountService getInstance(SavingsAccountDAO dao) {
		return new SavingsAccountServiceImpl(dao);
	}

}
