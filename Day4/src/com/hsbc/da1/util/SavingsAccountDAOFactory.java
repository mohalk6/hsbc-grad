package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArrayBackedDAOImpl;
import com.hsbc.da1.dao.ArrayListBackedDAOImpl;
import com.hsbc.da1.dao.LinkedListBackedDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;


public class SavingsAccountDAOFactory {
	
	public static SavingsAccountDAO getSavingsAccountDAO(int value) {
		switch(value) {
		case 1: return new ArrayBackedDAOImpl();
		case 2: return new ArrayListBackedDAOImpl();
		default: 
			return new LinkedListBackedDAOImpl();
		}
	}


}