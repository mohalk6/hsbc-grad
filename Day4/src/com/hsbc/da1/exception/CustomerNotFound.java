package com.hsbc.da1.exception;


public class CustomerNotFound extends Exception {
	
	public CustomerNotFound(String Message){
		super(Message);
	}
	
	@Override
	public String getMessage() {
		return  super.getMessage();
	}
}
