package com.hsbc.da1.exception;


public class InsufficientBankBalance extends Exception {
	
	public InsufficientBankBalance(String Message){
		super(Message);
	}
	
	@Override
	public String getMessage() {
		return  super.getMessage();
	}

}
