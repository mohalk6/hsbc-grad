package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.hsbc.da1.exception.CustomerNotFound;

import com.hsbc.da1.model.SavingsAccount;

public class ArrayListBackedDAOImpl implements SavingsAccountDAO {
	
	private List<SavingsAccount> savingsAccountList = new ArrayList<>();

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		this.savingsAccountList.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(SavingsAccount sa: savingsAccountList) {
			
		}
		return savingsAccount;
	
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		for (SavingsAccount sa: savingsAccountList) {
			if (sa.getAccountNumber() ==accountNumber) {
				this.savingsAccountList.remove(sa);
			}
		}		
	}


	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound{
		for(SavingsAccount sa: savingsAccountList) {
			if (sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		throw new CustomerNotFound("Customer Does not exist");		
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		return this.savingsAccountList;
		
	}


}



