package com.hsbc.da1.dao;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;;

public interface SavingsAccountDAO {

	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);

	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);

	public void deleteSavingsAccount(long accountNumber);
	
	List<SavingsAccount> fetchSavingsAccounts();
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound;

}
