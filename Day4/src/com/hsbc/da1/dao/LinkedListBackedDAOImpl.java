package com.hsbc.da1.dao;

import java.util.LinkedList;
import java.util.List;

import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.model.SavingsAccount;

public class LinkedListBackedDAOImpl implements SavingsAccountDAO {

	private List<SavingsAccount> savingsAccountLinkedList = new LinkedList<>();

	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for (SavingsAccount sa : savingsAccountLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
			}
		}
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) {
		for (SavingsAccount sa : savingsAccountLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				this.savingsAccountLinkedList.remove(sa);
			}
		}

	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return this.savingsAccountLinkedList;

	}


	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccountLinkedList.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound {
		for (SavingsAccount sa : savingsAccountLinkedList) {
			if (sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		throw new CustomerNotFound("Customer Does not exist");
	}

	

}
