package com.hsbc.da1.dao;

import java.util.Arrays;
import java.util.List;

import com.hsbc.da1.model.SavingsAccount;

public class ArrayBackedDAOImpl implements SavingsAccountDAO {

	private static SavingsAccount[] savingsAccounts = new SavingsAccount[100];
	private static int counter;

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {

		savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {

		for (int index = 0; index < savingsAccounts.length; index++) {

			if (savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {

		for (int index = 0; index < savingsAccounts.length; index++) {

			if (savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = null;
				break;
			}
		}
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {

		for (int index = 0; index < savingsAccounts.length; index++) {

			if (savingsAccounts[index].getAccountNumber() == accountNumber) {
				return savingsAccounts[index];
			}
		}
		return null;
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {
		return Arrays.asList(savingsAccounts);
	}

}
