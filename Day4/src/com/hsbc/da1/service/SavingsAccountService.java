package com.hsbc.da1.service;
import com.hsbc.da1.dao.*;
import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.exception.InsufficientBankBalance;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.model.Address;
import java.util.List;


public interface SavingsAccountService {


	public SavingsAccount createSavingsAccount(String customerName, long accountBalance,String city,String state, long pinCode);

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);

	public void deleteSavingsAccount(long accountNumber);
		
	public List<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound;
	
	public double withdraw(long accountId, double amount) throws CustomerNotFound, InsufficientBankBalance;
	
	public double deposit(long accoundId, double amount) throws CustomerNotFound;
	
	public double checkBalance(long accountId) throws CustomerNotFound;
	
	public double transfer(long senderAccountNumber,long toAccountNumber, double amount)throws InsufficientBankBalance,CustomerNotFound;


}


