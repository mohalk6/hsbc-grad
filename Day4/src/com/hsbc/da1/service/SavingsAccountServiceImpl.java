package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.dao.*;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;

import com.hsbc.da1.dao.ArrayBackedDAOImpl;
import com.hsbc.da1.dao.ArrayListBackedDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.CustomerNotFound;
import com.hsbc.da1.exception.InsufficientBankBalance;

public class SavingsAccountServiceImpl implements SavingsAccountService {

//	SavingsAccountDAO dao = new ArrayBackedDAOImpl();
	
	private SavingsAccountDAO dao;

	public SavingsAccountServiceImpl(SavingsAccountDAO saAccdao) {
		this.dao = saAccdao;
	}

	public SavingsAccount createSavingsAccount(String customerName, long accountBalance, String city, String state,
			long pinCode) {
		Address address = new Address(city, state, pinCode);
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, address);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance) {

		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

	public void deleteSavingsAccount(long accountNumber) {

		this.dao.deleteSavingsAccount(accountNumber);
	}

//	public SavingsAccount[] fetchSavingsAccount() {
//		
//		SavingsAccount[] accounts = this.dao.fetchSavingsAccounts();
//		return accounts;
//	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return this.dao.fetchSavingsAccounts();

	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound {

		SavingsAccount account = this.dao.fetchSavingsAccountByAccountId(accountNumber);
		return account;

	}

	public double withdraw(long accountId, double amount) throws CustomerNotFound, InsufficientBankBalance {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentBalance = savingsAccount.getAccountBalance();
			if (currentBalance > amount) {
				currentBalance = currentBalance - amount;
				savingsAccount.setAccountBalance(currentBalance);
				this.dao.updateSavingsAccount(accountId, savingsAccount);
				return currentBalance;
			} else {
				throw new InsufficientBankBalance("You Do Not Have Enough Bank Resources for transfer");
			}
		}
		return 0;
	}

	public double deposit(long accoundId, double amount) throws CustomerNotFound {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accoundId);
		if (savingsAccount != null) {
			double currentBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentBalance + amount);
			this.dao.updateSavingsAccount(accoundId, savingsAccount);
			return currentBalance;
		}

		return 0;
	}

	public double checkBalance(long accountId) throws CustomerNotFound {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}

		return 0;
	}

	public double transfer(long senderAccountNumber, long toAccountNumber, double amount)
			throws InsufficientBankBalance, CustomerNotFound {
		SavingsAccount sender = this.dao.fetchSavingsAccountByAccountId(senderAccountNumber);
		SavingsAccount to = this.dao.fetchSavingsAccountByAccountId(toAccountNumber);

		if (sender != null && to != null) {
			double updatedBalance = this.withdraw(senderAccountNumber, amount);

			if (updatedBalance > 0.00) {
				this.deposit(toAccountNumber, amount);
				return amount;
			}
		}
		return 0;

	}

}
