package com.hsbc.da1.controller;

import java.util.List;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.exception.*;
import com.hsbc.da1.service.SavingsAccountServiceImpl;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

public class SavingsAccountController {

	private SavingsAccountService savingsAccountService;

	public SavingsAccountController(SavingsAccountService savingsAccountService) {
		this.savingsAccountService = savingsAccountService;
	}

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance) {

		SavingsAccount savingsAccount = savingsAccountService.createSavingsAccount(customerName, accountBalance);
		return savingsAccount;
	}

	public SavingsAccount openSavingsAccount(String customerName, long accountBalance, String city, String state,
			long pinCode) {

		return this.savingsAccountService.createSavingsAccount(customerName, accountBalance, city, state, pinCode);

	}

	public void deleteSavingsAccount(long accountNumber) {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return this.savingsAccountService.fetchSavingsAccounts();

	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws CustomerNotFound {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountNumber, double amount) throws InsufficientBankBalance, CustomerNotFound {
		double amountCr = this.savingsAccountService.withdraw(accountNumber, amount);
		return amountCr;
	}

	public double deposit(long accountId, double amount) throws CustomerNotFound {
		return this.savingsAccountService.deposit(accountId, amount);
	}

	public double checkBalance(long accountId) throws CustomerNotFound {
		return this.savingsAccountService.checkBalance(accountId);
	}

	public void transfer(long accountId, long accountIdTo, double amount)
			throws InsufficientBankBalance, CustomerNotFound {
		this.savingsAccountService.transfer(accountId, accountIdTo, amount);
	}
}