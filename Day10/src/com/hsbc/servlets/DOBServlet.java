package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.model.User;

public class DOBServlet extends HttpServlet {

	@Override
	public void init() {
		System.out.println(" Inside the init method of the servlet");
	}

	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes) throws IOException {

//		String dobStr = httpServletReq.getParameter("dob");
//		
//		LocalDate dob = LocalDate.parse(dobStr, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
//		
//		LocalDateTime currentDate = LocalDateTime.now();
//		
//		Period period = Period.between(dob, LocalDate.now());
//		
//		User user = (User)httpServletReq.getSession().getAttribute("user");
//		
//		PrintWriter out = httpServletRes.getWriter();
//		
//		out.write("<h1> You are : " + period.getYears() +" years old </h1>");

		String dobStr = httpServletReq.getParameter("dob");

		PrintWriter out = httpServletRes.getWriter();

		User user = (User) httpServletReq.getSession().getAttribute("user");

		System.out.println(" User details in Age Calculator servlet " + user);

		// calculate the age in days ....
		int days = 340;

		out.write("<h1> You are  :" + days + " old!! </h1>");

	}
}
