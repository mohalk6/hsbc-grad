package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hsbc.model.User;

public class HelloWorldServlet extends HttpServlet {

	@Override
	public void init() {
		System.out.println(" Inside the init method of the servlet");
	}

	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes)
			throws IOException, ServletException {

		LocalDateTime currentDate = LocalDateTime.now();

//		PrintWriter out = httpServletRes.getWriter();
//		
//		out.write("<h1> The current Date is: " +currentDate.toString()+" </h1>");

//		if (httpServletReq.getParameter("fname") != null && 
//				httpServletReq.getParameter("lname") != null) {
//			RequestDispatcher rd = httpServletReq.getRequestDispatcher("/greet");
//			rd.forward(httpServletReq, httpServletRes);
//		} else {
//			httpServletRes.sendRedirect("age");
//		}	

		String flag = httpServletReq.getParameter("flag");

		User user = new User("Raman", 34);

		if (flag.equalsIgnoreCase("req")) {
			httpServletReq.setAttribute("username", "pradeep");
			httpServletReq.setAttribute("class", "Servlets & JSP");
			httpServletReq.setAttribute("user", user);
			RequestDispatcher rd = httpServletReq.getRequestDispatcher("/greet");
			rd.forward(httpServletReq, httpServletRes);
		} else if (flag.equalsIgnoreCase("session")) {
			HttpSession sessionScope = httpServletReq.getSession();
			sessionScope.setAttribute("username", "Kiran");
			sessionScope.setAttribute("class", "Java EE");
			sessionScope.setAttribute("user", user);
		} else {
			ServletContext context = httpServletReq.getServletContext();

			context.setAttribute("username", "Global User");
			context.setAttribute("class", "JAVA");
		}

	}
}
