package com.hsbc.servlets;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hsbc.model.User;

public class LogoutServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) {
		
		HttpSession session = req.getSession();
		User user = (User)session.getAttribute("user");
		
		System.out.println(" Logging out the user "+ user.getUsername());
		session.invalidate();
		
	}
	
	

}
