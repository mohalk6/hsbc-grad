package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hsbc.model.User;

public class GreetingsServlet extends HttpServlet {

	@Override
	public void init() {
		System.out.println(" Inside the init method of the servlet");
	}

	@Override
	public void doGet(HttpServletRequest httpServletReq, HttpServletResponse httpServletRes) throws IOException {

//		String firstName = httpServletReq.getParameter("fname");
//		
//		String lastName = httpServletReq.getParameter("lname");

		String reqUsername = (String) httpServletReq.getAttribute("username");
		String reqClass = (String) httpServletReq.getAttribute("class");
		System.out.println(" Req scope, Username " + reqUsername);
		System.out.println(" Req scope, Class " + reqClass);

		HttpSession session = httpServletReq.getSession();

		System.out.println("============== Session scope starts ===================");
		User user = (User) session.getAttribute("user");
		System.out.println(" Req scope, Username " + reqUsername);
		System.out.println(" Req scope, Class " + reqClass);
		System.out.println(" User  " + user);

		System.out.println(" Updating the state of the user ");
		session.removeAttribute("user");
		session.setAttribute("user", new User("Kiran", 44));

		System.out.println("============== Session scope ends ===================");

//		LocalDateTime currentDate = LocalDateTime.now();
//				
//		PrintWriter out = httpServletRes.getWriter();
//		
//		out.write("<h1> Welcome : " + firstName + " " + lastName + " Current time:"+currentDate.toString() + " </h1>");
	}
}
