package com.hsbc.hashset;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {
	public static void main(String []args) {
		Set<Integer> set = new HashSet<>();
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(23);
		set.add(33);
		set.add(44);
		set.add(55);
		
		System.out.println("Total number of elements in set: " + set.size());
		System.out.println(set.contains(23));
		
		Iterator<Integer> iterator = set.iterator();
		
		while(iterator.hasNext()) {
			int value = iterator.next();
			System.out.println("The value is " + value);
		}
		System.out.println(set.size());
	}
}
