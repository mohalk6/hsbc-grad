package com.hsbc.exceptions;
public class ExceptionDemo {
	
	public static void main(String[] args) {
		
		int a = 56;
		int b = 0;
		int array[] = {11, 22, 33, 44};
		try {
			System.out.println("Result of division is "+ (b/a));
			System.out.println("The last element of the array is "+array[3]);
			
			BaseClass obj = new DerivedClass();
			
			DerivedClass derived = (DerivedClass) obj;
			
		} catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
			System.out.println("Is it of Arithmetic type "+ (e instanceof ArithmeticException));
			System.out.println("Is it of ArrayIndexOutOfBoundsException type "+ (e instanceof ArrayIndexOutOfBoundsException));
			System.out.println(" Please enter a valid value for division "+ b +" is not allowed here ");
		} catch (ClassCastException e) {
			System.out.println("Cannot downcast with incompatible types");
		} catch(Exception e ) {
			System.out.println("Generic exception case");
		} catch (Throwable e){
			System.out.println("Throwable exception "+ e);
		}
		finally {
			System.out.println("This line will be printed if there is no exception");	
		}
		
	}
}


class BaseClass {
	
}

class DerivedClass extends BaseClass{
	
}

class AnotherDerivedClass extends BaseClass{
	
}
