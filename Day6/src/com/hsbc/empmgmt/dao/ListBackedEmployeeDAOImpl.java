package com.hsbc.empmgmt.dao;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

import com.hsbc.empmgmt.model.Employee;

public class ListBackedEmployeeDAOImpl implements EmployeeDAO {

	private static List<Employee> employeeList = new ArrayList<>();

	@Override
	public Employee saveEmployee(Employee employee) {
		this.employeeList.add(employee);
		return employee;
	}

	@Override
	public Employee updateEmployeeDetails(long employeeId, Employee employee) {

		for (Employee emp : employeeList) {
			if (emp.getEmployeeId() == employeeId) {
				emp = employee;
				break;
			}
		}
		return employee;

	}

	@Override
	public void removeEmployee(long employeeId) {

		for (Employee emp : employeeList) {
			if (emp.getEmployeeId() == employeeId) {
				this.employeeList.add(emp);
				break;
			}
		}
	}

	@Override
	public List<Employee> fetchEmployees() {
		return employeeList;
	}

	@Override
	public Employee fetchEmployeesByEmployeeId(long employeeId) {

		for (Employee emp : employeeList) {
			if (emp != null && emp.getEmployeeId() == employeeId) {
				return emp;
			}
		}
		return null;

	}

	@Override
	public Employee fetchEmployeeByEmployeeName(String employeeName) {

		for (Employee emp : employeeList) {
			if (emp != null && emp.getEmployeeName() == employeeName) {
				return emp;
			}
		}
		return null;
	}

}
