package com.hsbc.empmgmt.dao;

import java.util.Collection;
import java.util.List;

import com.hsbc.empmgmt.model.Employee;

public interface EmployeeDAO {
	
	public Employee saveEmployee(Employee employee );

	public Employee updateEmployeeDetails(long employeeId, Employee employee);

	public void removeEmployee(long employeeId);
	
	List <Employee> fetchEmployees();
	
	public Employee fetchEmployeesByEmployeeId(long accountNumber);
	
	public Employee fetchEmployeeByEmployeeName(String employeeName);

}
