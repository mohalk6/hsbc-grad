package com.hsbc.empmgmt.service;

import java.util.List;

import com.hsbc.empmgmt.dao.EmployeeDAO;
import com.hsbc.empmgmt.dao.ListBackedEmployeeDAOImpl;
import com.hsbc.empmgmt.model.Employee;
import com.hsbc.empmgmt.model.EmployeeLeavesRecord;
import com.hsbc.empmgmt.model.EmployeePersonalDetails;

public class EmployeeServiceImpl implements EmployeeService {

	EmployeeDAO dao = new ListBackedEmployeeDAOImpl();

	@Override
	public Employee createEmployeeRecord(String employeeName, String employeeDept, double employeeSalary) {

		Employee employee = new Employee(employeeName, employeeDept, employeeSalary);
		Employee saveEmployee = dao.saveEmployee(employee);
		return this.dao.saveEmployee(saveEmployee);
	}

	@Override
	public Employee createEmployeeRecord(String employeeName, String employeeDept, double employeeSalary,
			String employeeEmail, long employeeMobile) {

		EmployeePersonalDetails employeePersonalDetails = new EmployeePersonalDetails(employeeEmail, employeeMobile);
		Employee employee = new Employee(employeeName, employeeDept, employeeSalary, employeePersonalDetails);
		Employee saveEmployee = dao.saveEmployee(employee);
		return this.dao.saveEmployee(saveEmployee);
	}

	@Override
	public void deleteEmployeeRecord(long employeeId) {
		this.dao.removeEmployee(employeeId);
	}

	@Override
	public List<Employee> fetchEmployees() {

		List<Employee> employee = this.dao.fetchEmployees();
		return employee;

	}

	@Override
	public Employee fetchEmployeeByEmployeeId(long employeeId) {

		Employee employee = this.dao.fetchEmployeesByEmployeeId(employeeId);
		return employee;

	}

	@Override
	public Employee fetchEmployeeByEmployeeName(long employeeName) {

		Employee employee = this.dao.fetchEmployeesByEmployeeId(employeeName);
		return employee;
	}

	@Override
	public int applyForLeave(long employeeId, int requestedDaysOfLeaves) {

		Employee employee = this.dao.fetchEmployeesByEmployeeId(employeeId);
		if (employee != null) {
			EmployeeLeavesRecord employeeLeavesRecord = employee.getEmployeeLeavesRecord();
			if (requestedDaysOfLeaves < 10) {
				if (employeeLeavesRecord.getLeavesBalance() <= 40) {

					employeeLeavesRecord
							.setLeavesBalance(employeeLeavesRecord.getLeavesBalance() - requestedDaysOfLeaves);
					return employeeLeavesRecord.getLeavesBalance();
				}
			}

		}

		return 0;
	}

	@Override
	public int checkLeavesBalance(long employeeId) {

		Employee employee = this.dao.fetchEmployeesByEmployeeId(employeeId);
		if (employee != null) {
			EmployeeLeavesRecord employeeLeavesRecord = employee.getEmployeeLeavesRecord();
			return employeeLeavesRecord.getLeavesBalance();

		}

		return 0;
	}

}
