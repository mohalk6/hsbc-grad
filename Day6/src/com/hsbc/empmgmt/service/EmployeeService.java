package com.hsbc.empmgmt.service;

import java.util.List;

import com.hsbc.empmgmt.model.Employee;
import com.hsbc.empmgmt.model.EmployeePersonalDetails;


public interface EmployeeService {


	public Employee createEmployeeRecord(String employeeName, String employeeDept, double employeeSalary);

	public Employee createEmployeeRecord(String employeeName, String employeeDept, double employeeSalary,String employeeEmail, long employeeMobile);

	public void deleteEmployeeRecord(long employeeId);
		
	public List<Employee> fetchEmployees();

	public Employee fetchEmployeeByEmployeeId(long employeeId) ;
	
	public Employee fetchEmployeeByEmployeeName(long employeeName) ;
	
	public int applyForLeave(long employeeId, int requestedDaysOfLeaves);
	
	public int checkLeavesBalance(long employeeId);


}