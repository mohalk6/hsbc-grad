package com.hsbc.empmgmt.model;

public class EmployeePersonalDetails {

	private String employeeEmail;

	private long employeeMobile;


	public EmployeePersonalDetails(String employeeEmail, long employeeMobile) {

		this.employeeEmail = employeeEmail;
		this.employeeMobile = employeeMobile;
	}

	/**
	 * @return the employeeEmail
	 */
	public String getEmployeeEmail() {
		return employeeEmail;
	}

	/**
	 * @param employeeEmail the employeeEmail to set
	 */
	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	/**
	 * @return the employeeMobile
	 */
	public long getEmployeeMobile() {
		return employeeMobile;
	}

	/**
	 * @param employeeMobile the employeeMobile to set
	 */
	public void setEmployeeMobile(long employeeMobile) {
		this.employeeMobile = employeeMobile;
	}

}
