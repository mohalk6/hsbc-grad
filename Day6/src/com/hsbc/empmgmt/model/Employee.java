package com.hsbc.empmgmt.model;

public class Employee {

	private long employeeId;

	private String employeeName;

	private String employeeDept;

	private EmployeePersonalDetails employeePersonalDetails;

	private double employeeSalary;

	private EmployeeLeavesRecord employeeLeavesRecord;

	private static long employeeIdCounter = 1000;

	public Employee(String employeeName, String employeeDept, double employeeSalary) {

		this.employeeId = ++employeeIdCounter;
		this.employeeName = employeeName;
		this.employeeDept = employeeDept;
		this.employeeSalary = employeeSalary;
		this.employeeLeavesRecord = new EmployeeLeavesRecord();
	}

	public Employee(String employeeName, String employeeDept, double employeeSalary,
			EmployeePersonalDetails employeePersonalDetails) {

		this.employeeId = ++employeeIdCounter;
		this.employeeName = employeeName;
		this.employeeDept = employeeDept;
		this.employeeSalary = employeeSalary;
		this.employeePersonalDetails = employeePersonalDetails;
		this.employeeLeavesRecord = new EmployeeLeavesRecord();

	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Employee))
			return false;
		Employee other = (Employee) obj;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		return true;
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the employeeDept
	 */
	public String getEmployeeDept() {
		return employeeDept;
	}

	/**
	 * @param employeeDept the employeeDept to set
	 */
	public void setEmployeeDept(String employeeDept) {
		this.employeeDept = employeeDept;
	}

	/**
	 * @return the employeePersonalDetails
	 */
	public EmployeePersonalDetails getEmployeePersonalDetails() {
		return employeePersonalDetails;
	}

	/**
	 * @param employeePersonalDetails the employeePersonalDetails to set
	 */
	public void setEmployeePersonalDetails(EmployeePersonalDetails employeePersonalDetails) {
		this.employeePersonalDetails = employeePersonalDetails;
	}

	/**
	 * @return the employeeSalary
	 */
	public double getEmployeeSalary() {
		return employeeSalary;
	}

	/**
	 * @param employeeSalary the employeeSalary to set
	 */
	public void setEmployeeSalary(double employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	/**
	 * @return the employeeLeavesRecord
	 */
	public EmployeeLeavesRecord getEmployeeLeavesRecord() {
		return employeeLeavesRecord;
	}

	/**
	 * @param employeeLeavesRecord the employeeLeavesRecord to set
	 */
	public void setEmployeeLeavesRecord(EmployeeLeavesRecord employeeLeavesRecord) {
		this.employeeLeavesRecord = employeeLeavesRecord;
	}

	/**
	 * @return the employeeId
	 */
	public long getEmployeeId() {
		return employeeId;
	}

}
