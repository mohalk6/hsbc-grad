package com.hsbc.empmgmt.model;

public class EmployeeLeavesRecord {

	private long leaveId;

	private int leavesBalance;

	private static int leaveIdCounter = 101;
	

	public EmployeeLeavesRecord() {

		this.leaveId = leaveIdCounter++;
		this.leavesBalance = 40;

	}
	
	

	/**
	 * @param leavesBalance the leavesBalance to set
	 */
	public void setLeavesBalance(int leavesBalance) {
		this.leavesBalance = leavesBalance;
	}


	/**
	 * @return the leaveId
	 */
	public long getLeaveId() {
		return leaveId;
	}

	/**
	 * @return the leavesBalance
	 */
	public int getLeavesBalance() {
		return leavesBalance;
	}

}
