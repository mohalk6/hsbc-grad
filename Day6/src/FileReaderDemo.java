import java.io.*;

public class FileReaderDemo {

	public static void main(String[] args) {

		File textFile = new File("D://source.txt");

		readFile(textFile, "D://destination.txt");
	}

	private static void readFile(File textFile, String destFile) {

		try( 
			BufferedReader bufferedReader = new BufferedReader(new FileReader(textFile));
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(destFile)));
			){
			boolean endOfFile = false;
			while(!endOfFile) {
				String line = bufferedReader.readLine();
				if(line!=null) {
					bufferedWriter.append(line);
					bufferedWriter.newLine();
				}
				else {
					endOfFile = true;
				}
				
			}
		} catch (IOException e) {			
			e.printStackTrace();
		}

	}

}
