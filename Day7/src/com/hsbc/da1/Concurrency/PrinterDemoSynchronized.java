package com.hsbc.da1.Concurrency;
public class PrinterDemoSynchronized {
	public static void main(String[] args) {
		Printers printer1 = new Printers();
		
		Jobs job1= new Jobs(printer1, 10,20);
		Jobs job2= new Jobs(printer1, 30,40);
		Jobs job3= new Jobs(printer1, 45,50);
		
		Thread task1 = new Thread(job1);
		Thread task2 = new Thread(job2);
		Thread task3 = new Thread(job3);
		
		task1.start();
		task2.start();
		task3.start();
		
		try{
			task1.join();
			task3.join();
			task3.join();
		}
		catch(InterruptedException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("Completed all tasks!!!");
	}
	
}

class Printers{
	
	public synchronized void print(int start,int end) throws InterruptedException{
		if(end >= start) {
			System.out.println("Printing starts with " + start + " page!");
			for(int index = start;index<=end;index++) {
				System.out.println("Printing the " + index + " page");
				Thread.sleep(2000);;
			}
			System.out.println("Printing completed!!");
		}
	}
}
class Jobs implements Runnable{
	private Printers printer;
	int start;
	int end;
	
	public Jobs(Printers printer, int start,int end) {
		this.printer = printer;
		this.start = start;
		this.end = end;
	}
	
	@Override
	public void run() {
		try {
			printer.print(start, end);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

