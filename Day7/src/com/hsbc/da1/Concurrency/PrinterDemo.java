package com.hsbc.da1.Concurrency;

public class PrinterDemo {
	public static void main(String[] args) {
		Printer printer1 = new Printer();
		Printer printer2 = new Printer();
		Printer printer3 = new Printer();
		
		Job job1= new Job(printer1, 10,20);
		Job job2= new Job(printer2, 30,40);
		Job job3= new Job(printer3, 45,50);
		
		Thread task1 = new Thread(job1);
		Thread task2 = new Thread(job2);
		Thread task3 = new Thread(job3);
		
		task1.start();
		task2.start();
		task3.start();
		
		try{
			task1.join();
			task3.join();
			task3.join();
		}
		catch(InterruptedException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("Completed all tasks!!!");
	}
	

}


class Printer{
	
	public void print(int start,int end) throws InterruptedException{
		if(end >= start) {
			System.out.println("Printing starts with " + start + " page!");
			for(int index = start;index<=end;index++) {
				System.out.println("Printing the " + index + " page");
				Thread.sleep(2000);;
			}
			System.out.println("Printing completed!!");
		}
	}
}
class Job implements Runnable{
	private Printer printer;
	int start;
	int end;
	
	public Job(Printer printer, int start,int end) {
		this.printer = printer;
		this.start = start;
		this.end = end;
	}
	
	@Override
	public void run() {
		try {
			printer.print(start, end);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
