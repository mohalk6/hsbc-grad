package com.hsbc.da1.Thread;

public class ThreadAssignmentOne extends Thread {
	public static void main(String[] args) {
		ThreadAssignmentOne threadOne = new ThreadAssignmentOne();
		threadOne.setName("ThreadOne");
		threadOne.start();
		
		try {
			threadOne.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Main thread ended");
	}
	
	@Override
	public void run(){
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
