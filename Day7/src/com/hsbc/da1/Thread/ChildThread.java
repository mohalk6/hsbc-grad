package com.hsbc.da1.Thread;

public class ChildThread extends Thread {
	
	public static void main(String[] args) {
		System.out.println("Main thread when program starts : " + Thread.currentThread().getName());
		
		ChildThread t1 = new ChildThread();
		t1.setName("Thread1");
		
		ChildThread t2 = new ChildThread();
		t2.setName("Thread2");
		
		t1.start();
		t2.start();
		
		for(int index=0;index<5;index++) {
			System.out.println("Inside the main thread " + Thread.currentThread().getName());
			try {
				Thread.currentThread().sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("DO NOT DISTURB THE MAIN THREAD PLEASE!!!!");
			}
		}
		
		System.out.println("MAIN THREAD ENDED!!!");
		
	}
	
	@Override
	
	public void run() {
		System.out.println("=========Thread " + Thread.currentThread().getName() + " started ==========");
		
		for(int index=0;index<7;index++) {
			System.out.println("Name of the current thread " + Thread.currentThread().getName());
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("DO NT DISTURB " + Thread.currentThread().getName());
			}
		}
	}
	
	

}
