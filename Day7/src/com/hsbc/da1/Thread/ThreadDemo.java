package com.hsbc.da1.Thread;

public class ThreadDemo {
	public static void main(String[] args) {
		Thread t = Thread.currentThread();
		System.out.println("Current thread : " + t.getName());

		for (int index = 0; index < 10; index++) {
			System.out.println("The name of the current thread is " + t.getName() + " and its id is " + t.getId()
					+ "and its state is " + t.getState());
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("DO NOT DISTURB!!");
			}
		}
	}

}
