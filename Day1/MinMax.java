public class MinMax
{
    public static void main(String args[]){
        
        int[] inputArray = new int[args.length];
        int index=0;
        for(;index<args.length;index++){
            inputArray[index] = Integer.parseInt(args[index]);
        }

        System.out.println("Max Value is: "+ getMax(inputArray));
        System.out.println("Min Value is: "+ getMin(inputArray));
        
    }

    public static int getMax(int[] inputArray){ 
        int maxValue = inputArray[0]; 
        for(int i=1;i < inputArray.length;i++){ 
            if(inputArray[i] > maxValue){ 
                maxValue = inputArray[i]; 
            } 
        } 
        return maxValue; 
    }
 
    public static int getMin(int[] inputArray){ 
        int minValue = inputArray[0]; 
        for(int i=1;i<inputArray.length;i++){ 
            if(inputArray[i] < minValue){ 
                minValue = inputArray[i]; 
            } 
        } 
        return minValue; 
    } 
}