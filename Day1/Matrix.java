public class Matrix
{
    public static void main(String args[]){
        
        int[][] inputArray = new int[5][5];
        int rows = inputArray.length; 
        int cols = inputArray[0].length;
        
        initializeMatrix(inputArray);
        printMatrix(inputArray);
        
    }

    public static void initializeMatrix(int[][] inputArray){ 
        int intValue=20;
        for(int i=0;i<inputArray.length;i++){
            for(int j=0;j<inputArray[0].length;j++){
                inputArray[i][j]=intValue++;
            }
        }
            
    }
 
    public static void printMatrix(int[][] inputArray){ 
        int intValue=20;
        for(int i=0;i<inputArray.length;i++){
            for(int j=0;j<inputArray[0].length;j++){
                System.out.print(""+inputArray[i][j]+" ");
            }
             System.out.println("");
        }
            
    } 
}