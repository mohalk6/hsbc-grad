public class BillCalc{
    public static void main(String args[]){
        Double totalBill = Double.parseDouble(args[0]);
        String stateCode = args[1];
        Double taxValue;
        switch(stateCode)
        {
            case "KA":  taxValue = 0.15;
                break;
            case "TN" : taxValue = 0.18;
                break;
            case "MH" : taxValue = 0.20;
                break;
            default : taxValue = 0.12;
                break;
        }
        Double newTotalBill = totalBill + (totalBill*taxValue);
        System.out.println("Total Bill in " + stateCode + " is " + newTotalBill);
        

    }
}