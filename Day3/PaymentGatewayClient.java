interface PaymentGateway {
    
    void pay(String from, String to, double amount, String message);

}

interface MobileRecharge {

    void rechargeMobile(String phonenumber, double amount);

}

class PayPal implements PaymentGateway, MobileRecharge {
    public void pay(String from, String to, double amount, String message) {
        System.out.println("Payment from " + from + " to: " + to + " for an amount of " + amount + " Using PayPal" +"\n"+ "Message : " + message);
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge of amount " + amount + " has been made to " + phonenumber + " using PayPal ");
    }

}

class AmazonPay implements PaymentGateway, MobileRecharge {
    public void pay(String from, String to, double amount, String message) {
        System.out.println("Payment from " + from + " to: " + to + " for an amount of " + amount + " Using Amazon Pay." +"\n" +"Message : " + message);
    }

    public void rechargeMobile(String phonenumber, double amount) {
        System.out.println("Mobile recharge of amount " + amount + " has been made to " + phonenumber + " using AmazonPay");
    }
}

class Paytm implements PaymentGateway {
    public void pay(String from, String to, double amount, String message) {
        System.out.println(
                "Payment from " + from + " to: " + to + " for an amount of " + amount  + " using Paytm "+ "\n"+ "Message : " + message);
    }
}

public class PaymentGatewayClient {
    public static void main(String[] args) {

        PaymentGateway paymentGateway = null;
        MobileRecharge mobileRecharge = null;
        int payCode = Integer.parseInt(args[0]);
        if (payCode == 1) {
            PayPal payPal = new PayPal();
            paymentGateway = payPal;
            mobileRecharge = payPal;
            
        } 
        else if(payCode == 2){
            AmazonPay amazonPay = new AmazonPay();
            paymentGateway = amazonPay;
            mobileRecharge = amazonPay;
            
        }
        else if(payCode == 3){
            Paytm paytm = new Paytm();
            paymentGateway = paytm;
            mobileRecharge = null;
        }

        else
            System.out.println("Enter valid pay code");
        
        paymentGateway.pay("Riya", "Karan", 35000, "Payment for September'2020");
        if(mobileRecharge!=null)
            mobileRecharge.rechargeMobile("7798087142", 1299);
        
    }
}
