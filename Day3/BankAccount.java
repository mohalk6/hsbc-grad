
abstract class BankAccount{

    public static long accountNumberTracker = 10000;

	public String customerName;

	public long accountNumber;

    public double accountBalance;
    
    public BankAccount(String customerName, double accountBalance)
    {
        this.accountNumber = ++accountNumberTracker;
        this.customerName = customerName;
        this.accountBalance = accountBalance;
    }

    abstract void minBalance();
    
    abstract void withdrawl(double amount);

    abstract void loanEligibility(double amount);


	public double checkAccountBalance() {
		return this.accountBalance;
	}
	
	public final void deposit(double amount) {
		this.accountBalance =this.accountBalance + amount;
	}
    
}

final class CurrentAccount extends BankAccount{

    private String businessName;
	private String gstNumber;
    public CurrentAccount(String customerName, double accountBalance, String businessName, String gstNumber) {
        super(customerName, accountBalance);
        this.businessName = businessName;
        this.gstNumber = gstNumber;
    }

    void minBalance()
    {
        System.out.println("Minimum balance is 25,000");
        System.out.println("Current Balance is Rs." + checkAccountBalance());

    }

    void withdrawl(double amount)
    {
        System.out.println("No Withdrawal limit");
        System.out.println("balance before withdrawl Rs."+ checkAccountBalance());
        if(this.accountBalance > (amount + 25000))
            this.accountBalance = this.accountBalance-amount;
        System.out.println("balance after withdrawl Rs."+ checkAccountBalance());
        
    }

    void loanEligibility(double amount)
    {
        if(amount <= 2500000){

            System.out.println("Your Current Account is eligible for Loan of amount Rs." + amount);
            super.deposit(amount);
        }
			
		else 
			System.out.println("Your Current Account is not eligible for Loan for amount mot greater than Rs.25L.");
    }

}

final class SavingsAccount extends BankAccount {

    public SavingsAccount(String customerName, double accountBalance) {
        super(customerName, accountBalance);
    }

    void minBalance()
    {
        System.out.println("Minimum balance is 10,000");
        System.out.println("Current Balance is Rs." + checkAccountBalance());
    }

    void withdrawl(double amount)
    {
        System.out.println("Withdrawal limit is 10K");
        System.out.println("balance before withdrawl Rs."+ checkAccountBalance());
        if(this.accountBalance > (amount + 10000) && amount <= 10000)
            this.accountBalance = this.accountBalance-amount;
        System.out.println("balance after withdrawl Rs."+ checkAccountBalance());
    }

    void loanEligibility(double amount)
    {
        if(amount <= 500000){

            System.out.println("Your Savings Account is eligible for Loan of amount Rs." + amount);
            super.deposit(amount);
        }
			
		else 
			System.out.println("Your Savings Account is not eligible for Loan for amount not greater than Rs.5L. ");
    }

    // void applyForInsurance()
    // {
    //     InsuranceInterface insuranceInterface = new InsuranceInterface();
    // }
    // InsuranceInterface insurance = new InsuranceInterface();



}

final class SalariedAccount extends BankAccount{

    
    public SalariedAccount(String customerName, double accountBalance) {
        super(customerName, accountBalance);

    }    

    void minBalance()
    {
        System.out.println("No minimum balance required");
        System.out.println("Current Balance is Rs." + checkAccountBalance());
    }

    void withdrawl(double amount)
    {
        System.out.println("Withdrawal limit is 15K");
        System.out.println("balance before withdrawl Rs."+ checkAccountBalance());
        if(this.accountBalance >= amount && amount <= 15000)
            this.accountBalance = this.accountBalance-amount;
        System.out.println("balance after withdrawl Rs"+ checkAccountBalance());
    }

    void loanEligibility(double amount)
    {
        if(amount <= 1000000)
        {
            System.out.println("Your Salried Account is eligible for Loan of amount Rs." + amount);
            super.deposit(amount);
        }
			
		else 
			System.out.println("Your Salried Account is not eligible for Loan for amount not greater than Rs.10L. ");
    }


}

class BankClient{

    public static void main(String args[]){

        

        BankAccount account = null;
        int typeOfAccount = Integer.parseInt(args[0]);
        if (typeOfAccount == 1) {
            account = new CurrentAccount("Raj", 50000, "C&G", "123$ASDF");

        } else if (typeOfAccount == 2) {
            account = new SavingsAccount("Pooja", 155000);
            
        } else {
            account = new SalariedAccount("Karan", 106400);
        }

        execute(account);
    }

    public static void execute(BankAccount account) {
       
        account.minBalance();
        account.withdrawl(20000);
        account.loanEligibility(500000);
        


    }
}

