
interface Insurance{

    double calculatePremium(String vehicleName, double insuredAmount, String model);

    long applyForInsurance(double payPremium, String vehicleNumber, String model);
}

class BajajInsurance implements Insurance{

    static long counter = 1000;

    public long applyForInsurance(double payPremium, String vehicleNumber, String model){

        System.out.println("Your Insurance for vehicle " + vehicleNumber + "with a monthly premium of Rs." + payPremium
                + " over 4years");
        long policyNumber = Long.parseLong("BAJVI"+vehicleNumber+"_"+ counter);       
        return policyNumber;

    }

    @Override
    public double calculatePremium(String vehicleName, double insuredAmount, String model) {
        final double premiumAmount = insuredAmount*0.10;
        return premiumAmount;
    }
}

class TataAIGInsurance implements Insurance{

    static long counter = 2000;    
    
    public long applyForInsurance(double payPremium, String vehicleNumber, String model){

        System.out.println("Your Insurance for vehicle " + vehicleNumber + "with a monthly premium of Rs." + payPremium
                + " over 4years");
        long policyNumber = Long.parseLong("TATAVI"+vehicleNumber+"_"+ counter);       
        return policyNumber;

    }

    @Override
    public double calculatePremium(String vehicleName, double insuredAmount, String model) {
        double premiumAmount = 0;
        if(insuredAmount>500000)
        {
            premiumAmount = insuredAmount*0.10;
        }
        else{
            premiumAmount = insuredAmount*0.08;
        }
        return premiumAmount;
    }
}


public class InsuranceInterface {


    public static void main(String[] args) {

        Insurance insurance = null;
        int insuranceCode = Integer.parseInt(args[0]);
        if (insuranceCode == 1) {
            BajajInsurance bajajInsurance = new BajajInsurance();
            insurance = bajajInsurance;
        } 
        else{
            TataAIGInsurance tataInsurance = new TataAIGInsurance();
            insurance = tataInsurance;
        }

        double premium = insurance.calculatePremium("Activa 5G", 80000, "RED");
        System.out.println("Insurance Premium: Rs. " + premium);
        System.out.println("Policy Number: " + insurance.applyForInsurance(premium, "MH14JB4190", "RED"));

    }

    
}

